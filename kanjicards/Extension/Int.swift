//
//  TableViewDataSource.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import Foundation

extension Int
    {
        static func random(range: Range<Int> ) -> Int
        {
            var offset = 0
            
            if range.lowerBound < 0   // allow negative ranges
            {
                offset = abs(range.lowerBound)
            }
            
            let mini = UInt32(range.lowerBound + offset)
            let maxi = UInt32(range.upperBound + offset)
            
            return Int(mini + arc4random_uniform(maxi - mini)) - offset
        }
}
