//
//  TableViewDataSource.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import Foundation

extension String
{
    func random() -> String
    {
        let dice = Int.random(range: 0..<self.characters.count)
        if dice < self.characters.count {
           return String(self[self.index(self.startIndex, offsetBy: dice)])
        }
        else {
            return String(self[self.startIndex])
        }
    }
    
    func localized(lang:String) -> String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
    func localizedVi() -> String {
        return self.localized(lang: "vi")
    }
    
    func localizedDefault() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localized() -> String {
        return self.localizedDefault()
    }
}
