//
//  TableViewDataSource.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

//MARK: Apple-crayon
extension UIColor {
    static func cantaloupe() -> UIColor {
        return UIColor(red: 255, green: 206, blue: 110)
    }
    
    static func honeydew() -> UIColor {
        return UIColor(red: 206, green: 250, blue: 110)
    }
    
    static func spindrift() -> UIColor {
        return UIColor(red: 104, green: 251, blue: 208)
    }
    
    static func sky() -> UIColor {
        return UIColor(red: 106, green: 207, blue: 255)
    }
    
    static func lavender() -> UIColor {
        return UIColor(red: 210, green: 120, blue: 255)
    }
    
    static func carnation() -> UIColor {
        return UIColor(red: 255, green: 127, blue: 211)
    }
    
    static func licorice() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0)
    }
    
    static func snow() -> UIColor {
        return UIColor(red: 255, green: 255, blue: 255)
    }
    
    static func salmon() -> UIColor {
        return UIColor(red: 255, green: 114, blue: 110)
    }
    
    static func banana() -> UIColor {
        return UIColor(red: 255, green: 251, blue: 109)
    }
    
    static func flora() -> UIColor {
        return UIColor(red: 104, green: 249, blue: 110)
    }
    
    static func ice() -> UIColor {
        return UIColor(red: 104, green: 253, blue: 255)
    }
    
    static func orchid() -> UIColor {
        return UIColor(red: 110, green: 118, blue: 255)
    }
    
    static func bubblegum() -> UIColor {
        return UIColor(red: 255, green: 122, blue: 255)
    }
    
    static func lead() -> UIColor {
        return UIColor(red: 30, green: 30, blue: 30)
    }
    
    static func mercury() -> UIColor {
        return UIColor(red: 232, green: 232, blue: 232)
    }
    
    static func tangerine() -> UIColor {
        return UIColor(red: 255, green: 136, blue: 2)
    }
    
    static func lime() -> UIColor {
        return UIColor(red: 131, green: 249, blue: 2)
    }
    
    static func seaFoam() -> UIColor {
        return UIColor(red: 3, green: 249, blue: 135)
    }

    static func aqua() -> UIColor {
        return UIColor(red: 0, green: 140, blue: 255)
    }
    
    static func grape() -> UIColor {
        return UIColor(red: 137, green: 49, blue: 255)
    }
    
    static func strawberry() -> UIColor {
        return UIColor(red: 255, green: 41, blue: 135)
    }
    
    static func tungsten() -> UIColor {
        return UIColor(red: 58, green: 58, blue: 58)
    }
    
    static func silver() -> UIColor {
        return UIColor(red: 208, green: 208, blue: 208)
    }
    
    static func maraschino() -> UIColor {
        return UIColor(red: 255, green: 33, blue: 1)
    }
    
    static func lemon() -> UIColor {
        return UIColor(red: 255, green: 250, blue: 3)
    }
    
    static func spring() -> UIColor {
        return UIColor(red: 5, green: 248, blue: 2)
    }
    
    static func turquoise() -> UIColor {
        return UIColor(red: 0, green: 253, blue: 255)
    }
    
    static func blueberry() -> UIColor {
        return UIColor(red: 0, green: 46, blue: 255)
    }
    
    static func magenta() -> UIColor {
        return UIColor(red: 255, green: 57, blue: 255)
    }
    
    static func iron() -> UIColor {
        return UIColor(red: 84, green: 84, blue: 83)
    }
    
    static func magnesium() -> UIColor {
        return UIColor(red: 184, green: 184, blue: 184)
    }
    
    static func mocha() -> UIColor {
        return UIColor(red: 137, green: 72, blue: 0)
    }
    
    static func fern() -> UIColor {
        return UIColor(red: 69, green: 132, blue: 1)
    }
    
    static func moss() -> UIColor {
        return UIColor(red: 1, green: 132, blue: 72)
    }
    
    static func ocean() -> UIColor {
        return UIColor(red: 0, green: 74, blue: 136)
    }
    
    static func eggplant() -> UIColor {
        return UIColor(red: 73, green: 26, blue: 136)
    }
    
    static func maroon() -> UIColor {
        return UIColor(red: 137, green: 22, blue: 72)
    }
    
    static func steel() -> UIColor {
        return UIColor(red: 110, green: 110, blue: 110)
    }
    
    static func aluminum() -> UIColor {
        return UIColor(red: 160, green: 159, blue: 160)
    }
    
    static func cayenne() -> UIColor {
        return UIColor(red: 137, green: 17, blue: 0)
    }
    
    static func aspargus() -> UIColor {
        return UIColor(red: 136, green: 133, blue: 1)
    }
    
    static func clover() -> UIColor {
        return UIColor(red: 2, green: 132, blue: 1)
    }
    
    static func teal() -> UIColor {
        return UIColor(red: 0, green: 134, blue: 136)
    }
    
    static func midnight() -> UIColor {
        return UIColor(red: 0, green: 24, blue: 136)
    }
    
    static func plum() -> UIColor {
        return UIColor(red: 137, green: 30, blue: 136)
    }
    
    static func tin() -> UIColor {
        return UIColor(red: 135, green: 134, blue: 135)
    }
    
    static func nickel() -> UIColor {
        return UIColor(red: 136, green: 135, blue: 135)
    }
}
