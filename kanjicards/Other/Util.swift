//
//  Util.swift
//  BlinkTimer2
//
//  Created by Kha Nguyen on 4/11/16.
//  Copyright © 2016 Kha Nguyen. All rights reserved.
//

import UIKit

open class Util: NSObject {
    static let cardColors = ["orchid", "sky", "cantaloupe", "salmon", "grape", "aqua", "tangerine", "eggplant", "teal", "fern"]

    static let sharedInstance = Util()
    override fileprivate init() {} //This prevents others from using the default '()' initializer for this class.
    
    static open func colorForName(_ name: String) -> UIColor {
        return UIColor.perform(Selector(name)).takeUnretainedValue() as! UIColor
    }
    
    
}
