//
//  TableViewDataSource.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

struct K {
    struct NotificationKey {
        static let SettingChange = "kSettingChangeNotification"
    }
    
    struct ControllerIdentifer {
        static let LevelListViewController = "LevelListViewController"
        static let LessonListViewController = "LessonListViewController"
        static let WordListViewController = "WordListViewController"
        static let WordDetailViewController = "WordDetailViewController"
        static let QuizGameViewController = "QuizGameViewController"
        static let CompleteQuizPopupController = "CompleteQuizPopupController"
        static let SettingPopupViewController = "SettingPopupViewController"
        static let UserViewController = "UserViewController"
        static let SettingViewController = "SettingViewController"
        static let WordViewController = "WordViewController"
    }
    
    struct CellIdentifer {
        static let LevelCell = "LevelCell"
        static let LessonCell = "LessonCell"
        static let WordCell = "WordCell"
        static let WordDetailCell = "WordDetailCell"
        static let WordDetailExampleCell = "WordDetailExampleCell"
    }
    
    struct SegueIdentifer {
        static let ShowLessons = "showLessons"
    }
    
    struct HeaderFooterIdentifer {
        static let customHeader = "CustomHeader"
    }
    
    
    struct Collection {
        struct Collums {
            static let Level = 2
            static let Word = 3
        }
        struct Padding {
            static let CellSpacing = 5
            static let LineSpacing = 5
            static let SectionInsetsLeft = 5
            static let SectionInsetsRight = 5
            static let SectionInsetsTop = 5
            static let SectionInsetsBottom = 5
        }
    }
}

struct Const {
    static let UNCHANGE_CHARACTERS = "ぁぃぅぇぉっゎゕゖ"
    static let MAIN_CHARATERS = "あいうえお"
    static let SECONDARY_CHARACTERS = "かきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをがぎぐげござじずぜぞだぢづでどばびぶべぼぱぴぷぺぽ"
    static let UNCHANGE_PATTERN = "ゃゅょ"
    static let SECONDARY_PATTERN = "きしちにひみりぎぢじびぴ"
    static let TIME_ATTACK: TimeInterval = 0.01
    static let TIME_PER_WORD: TimeInterval = 7
}


enum AppState: Int {
    case normal = 0, quizSelection
}


enum QuizType: Int {
    case normal = 0, timeAttack
}

enum QuizLevel: Int {
    case basic = 0, intermediate, advanced
}

enum QuizState {
    case stop, running, pause, review
}


