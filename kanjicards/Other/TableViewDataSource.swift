//
//  TableViewDataSource.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class TableViewDataSource: NSObject, UITableViewDataSource {
 
    var arrayData:AnyObject?
    var cellIdentifer: String?
    var configureCell:((Any, Any) -> Void)?
    
    
    //MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (arrayData != nil) {
            return arrayData!.count
        }
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ((cellIdentifer != nil) && (arrayData != nil) && (configureCell != nil)) {
            let cell: AnyObject = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath)
            let item = arrayData!.objectAt(indexPath.row)
            configureCell!(cell, item)
            return cell as! UITableViewCell
        }
        return UITableViewCell()
    }
    
}
