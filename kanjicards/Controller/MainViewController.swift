//
//  MainViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 5/29/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit
import SideMenu

class MainViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    
    fileprivate var animationCompletion: (() -> Void)?
    fileprivate var viewControllers: [String] = [K.ControllerIdentifer.LevelListViewController,
                                                K.ControllerIdentifer.SettingViewController,
                                                K.ControllerIdentifer.UserViewController,
                                                K.ControllerIdentifer.SettingViewController,
                                                K.ControllerIdentifer.UserViewController,
                                                K.ControllerIdentifer.SettingViewController]
    fileprivate var activeViewController: UIViewController? {
        didSet {
            transitionViewController(fromController: oldValue)
        }
    }
    fileprivate var selectedIndex = 0
    lazy fileprivate var menuAnimator : MenuTransitionAnimator! = MenuTransitionAnimator(mode: .presentation, shouldPassEventsOutsideMenu: false) { [unowned self] in
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let currentController = storyboard!.instantiateViewController(withIdentifier: viewControllers[selectedIndex])
        activeViewController = currentController
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier, segue.destination) {
        case (.some("presentMenu"), let menu as MenuViewController):
            menu.selectedItem = selectedIndex
            menu.delegate = self
            menu.transitioningDelegate = self
            menu.modalPresentationStyle = .custom
        default:
            super.prepare(for: segue, sender: sender)
        }
    }
}

//MARK: Menu Delegete
extension MainViewController: MenuViewControllerDelegate {
    
    
    func menu(_: MenuViewController, didSelectItemAt index: Int, at point: CGPoint) {
        transitionPoint = point
        selectedIndex = index
        let currentController = storyboard!.instantiateViewController(withIdentifier: viewControllers[selectedIndex])
        activeViewController = currentController
        
        //Dismiss sidebar menu
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func menuDidCancel(_: MenuViewController) {
        dismiss(animated: true, completion: nil)
    }
}

extension MainViewController: UIViewControllerTransitioningDelegate {
    
    
    func animationController(forPresented presented: UIViewController, presenting _: UIViewController,
                             source _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return menuAnimator
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MenuTransitionAnimator(mode: .dismissal)
    }
}


//MARK: Container Manager + Transition Animation
extension MainViewController {
    fileprivate func transitionViewController(fromController: UIViewController?, withAnimation animation: Bool = true) {
        if let destinationController = activeViewController {
            addChildViewController(destinationController)
            containerView.addSubview(destinationController.view)
            destinationController.view.frame = containerView.bounds
            destinationController.didMove(toParentViewController: self)
            
            animationCompletion = {
                if let sourceController = fromController {
                    sourceController.willMove(toParentViewController: nil)
                    sourceController.view.removeFromSuperview()
                    sourceController.removeFromParentViewController()
                }
            }
            
            if animation {
                let screenBounds = UIScreen.main.bounds
                let targetView = destinationController.view!
                var centerPoint = CGPoint.zero
                if let _transitionPoint = transitionPoint {
                    centerPoint = targetView.convert(_transitionPoint, from: nil)
                }
                let radius: CGFloat = {
                    let x = max(centerPoint.x, screenBounds.width - centerPoint.x)
                    let y = max(centerPoint.y, screenBounds.height - centerPoint.y)
                    return sqrt(x * x + y * y)
                }()
                
                let animator = CircularRevealAnimator(layer: targetView.layer, center: centerPoint, startRadius: 0, endRadius: radius)
                animator.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
                animator.duration = 0.5
                animator.completion = { [unowned self] in
                    self.animationCompletion!()
                }
                animator.start()
            } else {
                self.animationCompletion!()
            }
        }
    }
}
