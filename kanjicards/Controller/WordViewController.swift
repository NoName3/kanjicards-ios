//
//  WordViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/30/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordViewController: BaseViewController {
    
    public var level: Level?
    public var currentWordIndex: Int?
    public var lesson: Lesson?
    
    @IBOutlet weak var cardsView: KCSwipeCardsView!
    var detailItems:[(key: String, value: String)] = []
    //MARK: Life cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _currentWordIndex = self.currentWordIndex {
            self.cardsView.currentCardIndex = _currentWordIndex
        }
        self.cardsView.numberOfVisibleCards = 3
        
        self.customNavigationBackButton()
        if let _level = self.level, let _lesson = self.lesson {
            levelRef.child(_level.key).child("lessons").child(_lesson.key).observe(.value, with: {[weak self] snapshot in
                let _lesson = Lesson(snapshot: snapshot)
                _lesson?.words?.sort(by: {$0.order < $1.order})
                if let _self = self {
                    _self.lesson = _lesson
                    _self.refreshUI() // need call after KCSwiftCardView had completed layout constraints
                }
            })
        }
    }
    
    
    override func removeCustomObserver() {
        super.removeCustomObserver()
        if let _level = self.level, let _lesson = self.lesson {
            levelRef.child(_level.key).child("lessons").child(_lesson.key).removeAllObservers()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.salmon()
    }
    
    
    func updateCardsDataSource() {
        if self.cardsView != nil {
            
            if let _wordCount = self.lesson?.words?.count {
                self.cardsView.numberOfCards = _wordCount
            }
            
            self.cardsView.cardAtIndex = { [unowned self] (index: Int) -> UIView in
                let card = self.cardsView.dequeueReusableCard(withIdentifier: "kanjiCard", for: index) as! KCCardView
                let frontCard = KanjiCardFrontView(frame: card.cardContainer.frame)
                let backCard = KanjiCardBackView(frame: card.cardContainer.frame)
                let word = self.lesson?.words?[index]
                
                let handleShowDetail = {[unowned self] in
                    let wordDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: K.ControllerIdentifer.WordDetailViewController) as! WordDetailViewController
                    wordDetailViewController.level = self.level
                    wordDetailViewController.lesson = self.lesson
                    wordDetailViewController.word = word
                    wordDetailViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                    self.navigationController?.present(wordDetailViewController, animated: true, completion: nil)
                    
                }
                
                frontCard.kanjiLabel.text = word?.kanji
                frontCard.cardNumberLabel.text = String(index + 1)
                frontCard.showDetail = nil
                backCard.kanjiLabel.text = word?.kanji
                backCard.hanLabel.text = word?.han
                backCard.shortMeanLabel.text = word?.shortMean
                backCard.kanjiNumberLabel.text = String(index + 1)
                backCard.showDetail = handleShowDetail
                card.frontCard = frontCard
                card.backCard = backCard
                card.cardColor = Util.colorForName(Util.cardColors[index])
                return card
            }
            self.cardsView.loadCards()
        }
    }
    
}


//MARK: Helper
extension WordViewController {
    
    fileprivate func refreshUI() {
        self.updateCardsDataSource()
    }
    
}
