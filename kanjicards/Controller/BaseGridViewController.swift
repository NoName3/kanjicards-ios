//
//  BaseGridViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/6/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class BaseGridViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    public var listItem: [ActionItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

