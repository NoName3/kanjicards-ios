//
//  UserViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 5/29/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.parent?.navigationItem.title = "user.navigation.title".localized()
    }

}
