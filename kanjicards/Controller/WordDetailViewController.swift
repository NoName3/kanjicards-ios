//
//  WordDetailViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/26/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordDetailViewController: BaseListViewController {
    
    public var level: Level?
    public var lesson: Lesson?
    public var word: Word? {
        didSet {
            self.refreshUI()
        }
    }
    
    var detailItems:[(key: String, value: String)] = []
    //MARK: Life cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationBackButton()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 44
        
        let nib = UINib(nibName: "CustomHeader", bundle: nil)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: K.HeaderFooterIdentifer.customHeader)
        
        if let _level = self.level, let _lesson = self.lesson, let _word = self.word {
            self.refreshUI()
            levelRef.child(_level.key).child("lessons").child(_lesson.key).child("words").child(_word.key).observe(.value, with: {[weak self] snapshot in
                let _word = Word(snapshot: snapshot)
                if let _self = self {
                    _self.word = _word
                }
            })
        }
    }
    
    override func removeCustomObserver() {
        super.removeCustomObserver()
        if let _level = self.level, let _lesson = self.lesson, let _word = self.word {
            levelRef.child(_level.key).child("lessons").child(_lesson.key).child("words").child(_word.key).removeAllObservers()
        }
    }
    
    
    @IBAction func closeButtonDidTouch(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: TableView Datasource
extension WordDetailViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.detailItems.count
        } else {
            if let word = self.word {
                return (word.example?.count)!
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifer.WordDetailCell, for: indexPath) as! WordDetailCell
            let item = self.detailItems[indexPath.row]
            cell.keyLabel.text = item.key
            cell.valueLabel.text = item.value
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifer.WordDetailExampleCell, for: indexPath) as! WordDetailExampleCell
            let item = self.word?.example?[indexPath.row]
            cell.kanjiLabel.text = item?.kanji
            if let _hiragana = item?.hiragana {
                cell.hiraganaLabel.text = _hiragana
            }
            var meanString:String = ""
            if let _han = item?.han {
                meanString += "(\(_han))"
            }
            if let _shortMean = item?.shortMean {
                meanString += " \(_shortMean)"
            }
            cell.shortMeanLabel.text = meanString
            return cell
        }
    }
}


//MARK: TableView Delegate
extension WordDetailViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            let title = "word.detail.example".localized()
            
            // Dequeue with the reuse identifier
            let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: K.HeaderFooterIdentifer.customHeader)
            let header = cell as! CustomHeader
            header.themeView.backgroundColor = UIColor.salmon()
            header.titleLabel.text = title
            header.titleLabel.textColor = UIColor.white
            
            return cell
        }
        
    }
    
}


//MARK: Helper
extension WordDetailViewController {
    
    fileprivate func refreshUI() {
        self.setScreenTitle(title: word?.kanji)
        if tableView != nil {
            if self.tableViewDataSource != nil && self.tableView != nil {
                var items:[(key: String, value: String)] = []
                if let _word = self.word {
                    items += [(key: "word.detail.kanji".localized(), value: _word.kanji)]
                    if let _han = _word.han {
                        items += [(key: "word.detail.han".localized(), value: _han)]
                    }
                    items += [(key: "word.detail.kun".localized(), value: _word.kunString())]
                    items += [(key: "word.detail.on".localized(), value: _word.onString())]
                    if let _shortMean = _word.shortMean {
                        items += [(key: "word.detail.shortmean".localized(), value: _shortMean)]
                    }
                    items += [(key: "word.detail.means".localized(), value: _word.meanString())]
                }
                self.detailItems = items
                self.tableViewDataSource?.arrayData = self.detailItems as AnyObject?
                self.tableView.reloadData()
            }
        }
    }
    
}

