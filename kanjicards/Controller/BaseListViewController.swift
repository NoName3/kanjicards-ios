//
//  BaseListViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class BaseListViewController: BaseViewController {

    var tableViewDataSource:TableViewDataSource?
    public var listItem: [ActionItem] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTable()
    }
    
}


//MARK: Helper
extension BaseListViewController {
    
    public func configureTable() {
        self.tableViewDataSource = TableViewDataSource()
        self.tableView.dataSource = self.tableViewDataSource
    }
    
}
