//
//  LevelListViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class LevelListViewController: BaseGridViewController {
    
    fileprivate var levels: [Level] = [] {
        didSet {
            self.refreshListItem()
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.navigationItem.title = "level.list.navigation.title".localized()
        levelRef.queryOrdered(byChild: "jlpt").observe(.value, with: {[weak self] snapshot in
            var newItems: [Level] = []
            for item in snapshot.children {
                if let _item = Level(snapshot: item as! FIRDataSnapshot) {
                    newItems.append(_item)
                }
            }
            if let _self = self {
                _self.levels = newItems
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.sky()
    }
    
    
    override func removeCustomObserver() {
        super.removeCustomObserver()
        levelRef.removeAllObservers()
    }
    
}

//MARK: CollectionView Datasource
extension LevelListViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItem.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: K.CellIdentifer.LevelCell, for: indexPath) as! LevelCell
        let item = listItem[indexPath.row]
        cell.value = item;
        return cell
    }
    
}

//MARK: CollectionView Delegate
extension LevelListViewController : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.size
        let cellSize = Int((screenSize.width - CGFloat(K.Collection.Padding.SectionInsetsLeft + K.Collection.Padding.SectionInsetsRight + (K.Collection.Padding.CellSpacing * (K.Collection.Collums.Level - 1)))) / CGFloat(K.Collection.Collums.Level))
        return CGSize(width: cellSize, height: cellSize)
    }
}


extension LevelListViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let attributes = collectionView.layoutAttributesForItem(at: indexPath)
        transitionPoint = collectionView.convert((attributes?.center)!, to: nil)
        
        let item = listItem[indexPath.row]
        if let _action = item.action {
            _action(item)
        }
    }
    
}


//MARK: Helper
extension LevelListViewController {
    
    
    func refreshListItem() {
        listItem = []
        for level in levels {
            let actionItem = ActionItem()
            actionItem.title = "N\(String(level.jlpt))"
            actionItem.data = level
            actionItem.action = {[unowned self] (item: ActionItem) -> Void in
                let lessonListViewController = self.storyboard?.instantiateViewController(withIdentifier: K.ControllerIdentifer.LessonListViewController) as! LessonListViewController
                lessonListViewController.level = item.data as? Level
                self.navigationController?.pushViewController(lessonListViewController, animated: true)
            }
            listItem.append(actionItem)
        }
        if !(listItem.count % K.Collection.Collums.Level == 0 ){
            listItem.append(ActionItem())
        }
    }
    
}

