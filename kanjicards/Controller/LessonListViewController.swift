//
//  LessonListViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class LessonListViewController: BaseListViewController {
    
    public var level: Level? {
        didSet {
            self.refreshListItem()
            self.refreshUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBackButton()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapHandle(sender:)))
        singleTap.delegate = self
        self.tableView.addGestureRecognizer(singleTap)
        self.tableView.rowHeight = 80
        self.tableView.dataSource = self;
        
        self.setScreenTitle(title: "lesson.list.navigation.title".localized())
        if let _level = self.level {
            self.refreshUI()
            levelRef.child(_level.key).observe(.value, with: {[weak self] snapshot in
                let _level = Level(snapshot: snapshot)
                _level?.lessons?.sort(by: {$0.order < $1.order})
                if let _self = self {
                    _self.level = _level
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.aqua()
    }
    
    override func removeCustomObserver() {
        super.removeCustomObserver()
        if let _level = self.level {
            levelRef.child(_level.key).removeAllObservers()
        }
    }
    
}


//MARK: Action 
extension LessonListViewController {
    
    
    func tapHandle(sender: UITapGestureRecognizer) {
        let tableView = sender.view as! UITableView
        let touchPoint = sender.location(in: tableView)
        transitionPoint = tableView.convert(touchPoint, to: nil)
        
        let indexPath = tableView.indexPathForRow(at: touchPoint)
        let item = listItem[(indexPath?.row)!]
        if let _action = item.action {
            _action(item)
        }
    }
    
}


//MARK: TableView Datasource
extension LessonListViewController : UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifer.LessonCell, for: indexPath) as! LessonCell
        let item = listItem[indexPath.row]
        cell.value = item
        return cell
    }
    
}


//MARK: GestureRecognizer Delegate
extension LessonListViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let tableView = gestureRecognizer.view as! UITableView
        let touchPoint = gestureRecognizer.location(in: tableView)
        if (tableView.indexPathForRow(at: touchPoint) != nil) {
            return true
        }
        return false
    }
}


//MARK: Helper
extension LessonListViewController {
    
    fileprivate func refreshUI() {
        if self.level != nil {
            self.setScreenTitle(title: "N\(self.level!.jlpt)")
        } else {
            self.setScreenTitle(title: "")
        }
        
        if self.tableViewDataSource != nil && self.tableView != nil {
            self.tableViewDataSource?.arrayData = listItem as AnyObject?
            self.tableView.reloadData()
        }
    }
    
    func refreshListItem() {
        listItem = []
        for lesson in (level?.lessons)! {
            let actionItem = ActionItem()
            actionItem.title = lesson.title
            let subTitle = Lesson.descriptionOfLesson(lesson: lesson)
            actionItem.subTitle = subTitle.0
            actionItem.subTitle2 = subTitle.1
            actionItem.data = lesson
            actionItem.action = {[unowned self] (item: ActionItem) -> Void in
                let wordListViewController = self.storyboard?.instantiateViewController(withIdentifier: K.ControllerIdentifer.WordListViewController) as! WordListViewController
                wordListViewController.level = self.level
                wordListViewController.lesson = item.data as? Lesson
                self.navigationController?.pushViewController(wordListViewController, animated: true)
            }
            listItem.append(actionItem)
        }
    }
    
}

