//
//  WordListViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/26/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordListViewController: BaseGridViewController {

    public var level: Level?
    public var lesson: Lesson? {
        didSet {
            self.refreshUI()
        }
    }
    
    
    //MARK: Life cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBackButton()
        
        self.setScreenTitle(title: "word.list.navigation.title".localized())
        if let _level = self.level, let _lesson = self.lesson {
            self.refreshUI()
            levelRef.child(_level.key).child("lessons").child(_lesson.key).observe(.value, with: {[weak self] snapshot in
                let _lesson = Lesson(snapshot: snapshot)
                _lesson?.words?.sort(by: {$0.order < $1.order})
                if let _self = self {
                    _self.lesson = _lesson
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.fern()
    }
    
    override func removeCustomObserver() {
        super.removeCustomObserver()
        if let _level = self.level, let _lesson = self.lesson {
            levelRef.child(_level.key).child("lessons").child(_lesson.key).removeAllObservers()
        }
    }

}


//MARK: CollectionView Datasource
extension WordListViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItem.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: K.CellIdentifer.WordCell, for: indexPath) as! WordCell
        let item = listItem[indexPath.row]
        cell.value = item;
        return cell
    }
    
}

//MARK: CollectionView Delegate
extension WordListViewController : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.size
                let cellSize = Int((screenSize.width - CGFloat(K.Collection.Padding.SectionInsetsLeft + K.Collection.Padding.SectionInsetsRight + (K.Collection.Padding.CellSpacing * (K.Collection.Collums.Word - 1)))) / CGFloat(K.Collection.Collums.Word))
        return CGSize(width: cellSize, height: cellSize)
    }
}


extension WordListViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let attributes = collectionView.layoutAttributesForItem(at: indexPath)
        transitionPoint = collectionView.convert((attributes?.center)!, to: nil)
        
        let item = listItem[indexPath.row]
        if let _action = item.actionAtIndex {
            _action(item, indexPath)
        }
    }
    
}



//MARK: Helper
extension WordListViewController {
    
    func refreshListItem() {
        listItem = []
        for word in (lesson?.words)! {
            let actionItem = ActionItem()
            actionItem.title = word.kanji
            actionItem.data = word
            actionItem.actionAtIndex = {[unowned self] (action: ActionItem, index: IndexPath) -> Void in
                let wordViewController = self.storyboard?.instantiateViewController(withIdentifier: K.ControllerIdentifer.WordViewController) as! WordViewController
                wordViewController.level = self.level
                wordViewController.lesson = self.lesson
                wordViewController.currentWordIndex = index.row
                self.navigationController?.pushViewController(wordViewController, animated: true)

            }
            listItem.append(actionItem)
        }
        var remainItem = K.Collection.Collums.Word - Int(listItem.count % K.Collection.Collums.Word)
        while remainItem != 0 {
            listItem.append(ActionItem())
            remainItem -= 1
        }
    }
    
    fileprivate func refreshUI() {
        self.setScreenTitle(title: lesson?.title)
        self.refreshListItem()
        if (self.collectionView != nil) {
            self.collectionView.reloadData()
        }
    }
    
}
