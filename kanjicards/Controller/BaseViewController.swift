//
//  BaseViewController.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var transitionPoint: CGPoint?
    
    let levelRef = FIRDatabase.database().reference(withPath: "levels")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.delegate = self
    }
    
    deinit {
        self.removeCustomObserver()
    }
    
}


//MARK: UINavigationController Delegate
extension BaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_: UINavigationController, animationControllerFor _: UINavigationControllerOperation,
                              from _: UIViewController, to _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        var centerPoint = CGPoint.zero
        if let _transitionPoint = transitionPoint {
            centerPoint = _transitionPoint
        }
        return CircularRevealTransitionAnimator(center: centerPoint)
    }
}

//MARK: Helper
extension BaseViewController {
    
    func setScreenTitle(title: String?) -> Void {
        if let _title = title {
            self.navigationItem.title = _title
        } else {
            self.navigationItem.title = ""
        }
    }
    
    
    func removeCustomObserver() {
        // Remove custom observer
    }
    
    
    func customNavigationBackButton() {
        let newBackButton = UIBarButtonItem(image: UIImage(named: "nav-back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backHandle(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func backHandle(sender: UIBarButtonItem) {
        transitionPoint = CGPoint.zero
        _ = navigationController?.popViewController(animated: true)
    }
    
}
