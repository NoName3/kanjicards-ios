//
//  FirebaseModel.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class FirebaseModel: NSObject {
    var key: String
    var ref: FIRDatabaseReference?
    
    override init() {
        self.key = ""
    }
    
    init?(snapshot: FIRDataSnapshot) {       
        self.key = snapshot.key
        self.ref = snapshot.ref
    }
}
