//
//  Word.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class Word: FirebaseModel {

    var kanji: String
    var hiragana:String?
    var han: String?
    var kun:[String]?
    var on:[String]?
    var strokes: Int?
    var mean:[String]?
    var shortMean:String?
    var example:[Word]?
    var order:Int
    
    init(kanji: String, hiragana: String) {
        self.kanji = kanji
        self.hiragana = hiragana
        self.order = 0
        super.init()
    }
    
    init(kanji: String, hiragana: String?, han:String?, kun:[String]?, on:[String]?, strokes:Int?, mean:[String]?, example:[Word]?, shortMean:String?) {
        self.order = 0
        self.kanji = kanji
        if let _hiragana = hiragana {
            self.hiragana = _hiragana
        }
        if let _han = han {
            self.han = _han
        }
        if let _kun = kun {
            self.kun = _kun
        }
        if let _on = on {
            self.on = _on
        }
        if let _strokes = strokes {
            self.strokes = _strokes
        }
        if let _mean = mean {
            self.mean = _mean
        }
        if let _example = example {
            self.example = _example
        }
        if let _shortMean = shortMean {
            self.shortMean = _shortMean
        }
        super.init()
    }
    
    override init?(snapshot: FIRDataSnapshot) {
        guard let dict = snapshot.value as? [String: AnyObject] else { return nil }
        guard let kanji = dict["kanji"] else { return nil }
        let exampleData  = snapshot.childSnapshot(forPath: "example") as FIRDataSnapshot
        var exampleItems: [Word] = []
        for item in exampleData.children {
            let exampleItem = Word(snapshot: item as! FIRDataSnapshot)
            if let _exampleItem = exampleItem {
                exampleItems.append(_exampleItem)
            }
        }
        example = exampleItems
        
        self.kanji = kanji as! String
        
        if let _hiragana = dict["hiragana"] {
            self.hiragana = _hiragana as? String
        }
        if let _han = dict["han"] {
            self.han = _han as? String
        }
        if let _kun = dict["kun"] {
            self.kun = _kun as? [String]
        }
        if let _on = dict["on"] {
            self.on = _on as? [String]
        }
        if let _strokes = dict["strokes"] {
            self.strokes = _strokes as? Int
        }
        if let _mean = dict["mean"] {
            self.mean = _mean as? [String]
        }
        if let _shortMean = dict["shortMean"] {
            self.shortMean = _shortMean as? String
        }
        self.order = 0
        if let _order = dict["order"] {
            if (_order is NSNumber) {
                self.order = _order as! Int
            }
        }
        super.init(snapshot: snapshot)
    }
}

extension Word: NSCopying
{
    
    override func isEqual(_ object: Any?) -> Bool {
        if let other = object as? Word {
            return other.kanji == self.kanji
        }
        return false
    }
    
    override var hash: Int {
        return key.hashValue
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Word(kanji: self.kanji, hiragana: self.hiragana, han: self.han, kun: self.kun, on: self.on, strokes: self.strokes, mean: self.mean, example: self.example, shortMean: self.shortMean)
    }
    
    func kunString() -> String {
        if let _kun = self.kun {
            return _kun.joined(separator: "、")
        }
        return ""
    }
    
    func onString() -> String {
        if let _on = self.on {
            return _on.joined(separator: "、")
        }
        return ""
    }
    
    func meanString() -> String {
        if let _mean = self.mean {
            return _mean.joined(separator: "\n")
        }
        return ""
    }
    
}
