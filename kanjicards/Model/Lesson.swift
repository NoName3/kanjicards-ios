//
//  Lesson.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class Lesson: FirebaseModel {
    
    var title:String
    var order:Int
    var words:[Word]?
    
    init(title:String, words:[Word]?) {
        self.title = title
        self.words = words
        self.order = 0
        super.init()
    }
    
    override init?(snapshot: FIRDataSnapshot) {
        guard let dict = snapshot.value as? [String: AnyObject] else { return nil }
        guard let title = dict["title"] else { return nil }
        let wordData  = snapshot.childSnapshot(forPath: "words") as FIRDataSnapshot
        var wordItems: [Word] = []
        for item in wordData.children {
            let wordItem = Word(snapshot: item as! FIRDataSnapshot)
            if let _wordItem = wordItem {
                wordItems.append(_wordItem)
            }
        }
        words = wordItems
        
        self.title = title as! String
        
        self.order = 0
        if let _order = dict["order"] {
            if (_order is NSNumber) {
                self.order = _order as! Int
            }
        }
        
        super.init(snapshot: snapshot)
    }
}

extension Lesson: NSCopying
{
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Lesson(title: self.title, words: self.words)
    }
    

    static func descriptionOfLesson(lesson:Lesson) -> (String, String) {
        var description1 = ""
        var description2 = ""
        var sampleWords = [String]();
        if let _words = lesson.words {
            for word in _words {
                sampleWords.append(word.kanji)
            }
            if sampleWords.count > 0 {
                let splitSample = sampleWords.splitHalf()
                description1 = splitSample.left.joined(separator: ", ")
                description2 = splitSample.right.joined(separator: ", ")
            }
        }
        return (description1, description2)
    }
}
