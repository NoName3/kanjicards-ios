//
//  Level.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/25/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit
import Firebase

class Level: FirebaseModel {
    var jlpt: Int
    var lessons: [Lesson]?
    

    init(jlpt: Int, lessons: [Lesson]?) {
        self.jlpt = jlpt
        self.lessons = lessons
        super.init()
    }
    
    override init?(snapshot: FIRDataSnapshot) {
        guard let dict = snapshot.value as? [String: AnyObject] else { return nil }
        guard let jlpt = dict["jlpt"] else { return nil }
        let lessonData  = snapshot.childSnapshot(forPath: "lessons") as FIRDataSnapshot
        var lessonItems: [Lesson] = []
        for item in lessonData.children {
            let lessonItem = Lesson(snapshot: item as! FIRDataSnapshot)
            if let _lessonItem = lessonItem {
                lessonItems.append(_lessonItem)
            }
        }
        lessons = lessonItems
        
        self.jlpt = jlpt as! Int
        
        super.init(snapshot: snapshot)
    }
}


extension Level: NSCopying
{
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Level(jlpt:self.jlpt, lessons:self.lessons)
    }
    
    static func descriptionOfLevel(level:Level) -> String {
        var description = ""
        var totalWords = 0
        if let _lessons = level.lessons {
            for lesson in _lessons {
                if let _words = lesson.words {
                    totalWords += _words.count
                }
            }
            description = String.init(format: "level.cell.subtitle".localized(), _lessons.count, totalWords)
        }

        return description;
    }
}
