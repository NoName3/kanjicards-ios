//
//  KCCardView.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/20/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

@IBDesignable class KCCardView: UIView {
    
    enum State {
        case  idle, new, removing, animating
    }
    
    enum showCase {
        case front, back
    }
    
    public var frontCard: KCCardContentView?
    public var backCard: KCCardContentView?
    
    
    var state: State = .new
    var show: showCase = .front
    
    @IBInspectable var cardCornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = self.cardCornerRadius
        }
    }
    
    @IBInspectable var cardColor: UIColor = UIColor.white {
        didSet {
                backgroundColor = self.cardColor
        }
    }
    
    @IBInspectable var cardIdentifier: String = ""
    
    @IBOutlet weak var cardContainer: UIView!
    
    
    
    // Our custom view from the XIB file
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    // MARK: Life cycle
    required public override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }

    
    fileprivate func setup() {
       
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 50.0
        self.cardContainer.backgroundColor = UIColor.clear
    }
    
    open func prepareForReuse() {
        
    }
    
    func showFrontView()  {
        applyShowType(show: .front)
    }
    
    
    func attachView() {
        showFrontView()
    }
    
    
    func switchContentView()  {
        switch show {
        case .front:
            applyShowType(show: .back)
        case .back:
            applyShowType(show: .front)
        }
    }
    
}


//MARK: Helper
extension KCCardView {
    fileprivate func applyShowType(show: showCase) {
        for view in self.cardContainer.subviews {
            view.removeFromSuperview()
        }
        switch show {
        case .front:
            if let _frontCard = self.frontCard {
                self.show = .front
                _frontCard.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
                _frontCard.frame.origin = CGPoint.zero
                self.cardContainer.addSubview(_frontCard)
            }
        case .back:
            if let _backCard = self.backCard {
                self.show = .back
                _backCard.frame = self.cardContainer.bounds
                self.cardContainer.addSubview(_backCard)
            }
        }
    }
}


extension KCCardView: NSCopying
{
    
    func copy(with zone: NSZone? = nil) -> Any {
        let classType = type(of: self)
        let cloneCard = classType.init(frame: self.frame)
        cloneCard.cardColor = self.cardColor
        cloneCard.cardIdentifier = self.cardIdentifier
        cloneCard.cardCornerRadius = self.cardCornerRadius
        return cloneCard
    }
}
