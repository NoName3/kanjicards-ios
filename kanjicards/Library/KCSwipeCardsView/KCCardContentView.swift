//
//  KCCardContentView.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/23/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class KCCardContentView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        return UIView()
    }
    
    // MARK: Life cycle
    required public override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    public func setup() {
        
        view.backgroundColor = UIColor.clear
    }

    
}
