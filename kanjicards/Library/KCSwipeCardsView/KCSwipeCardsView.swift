//
//  KCSwipeCardView.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/19/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

// MARK: - Main
@IBDesignable class KCSwipeCardsView: UIView {
   
    @IBOutlet weak var containerView: UIView!
    
    fileprivate var tapGestureRecognizer: UITapGestureRecognizer?
    fileprivate var swipeLeftGestureRecognizer: UISwipeGestureRecognizer?
    fileprivate var swipeRightGestureRecognizer: UISwipeGestureRecognizer?
    
    public var currentCardIndex:Int = 0 {
        didSet {
            if currentCardIndex < 0 {
                    currentCardIndex = 0
            }
        }
    }
    public var numberOfCards: Int = 0 {
        didSet {
            if numberOfCards < 0 {
                numberOfCards = 0
            }
        }
    }
    public var numberOfVisibleCards: Int = 0 {
        didSet {
            if numberOfVisibleCards < 0 {
                numberOfVisibleCards = 0
            }
        }
    }
    public var cardAtIndex:((_ index: Int) -> UIView)?
    public var animateArrangeView = KCSwipeCardsView.arrangeAnimator()
    public var animateFlipView = KCSwipeCardsView.flipAnimator()
    public var visibleCards = [KCCardView]()
    
    
    
    // Our custom view from the XIB file
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "KCSwipeCardsView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    // MARK: Life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        // 3. Setup view from .xib file
        xibSetup()
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // 3. Setup view from .xib file
        xibSetup()
        setup()
    }
    
    fileprivate func setup() {
        self.swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeLeft(gesture:)))
        self.swipeLeftGestureRecognizer?.direction = .left
        self.addGestureRecognizer(self.swipeLeftGestureRecognizer!)
        
        self.swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeRight(gesture:)))
        self.swipeRightGestureRecognizer?.direction = .right
        self.addGestureRecognizer(self.swipeRightGestureRecognizer!)
        
        
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        self.addGestureRecognizer(self.tapGestureRecognizer!)
    }
    
    deinit {
        self.removeGestureRecognizer(self.tapGestureRecognizer!)
        self.removeGestureRecognizer(self.swipeLeftGestureRecognizer!)
        self.removeGestureRecognizer(self.swipeRightGestureRecognizer!)
    }
    
    
}


// MARK: Private function
extension KCSwipeCardsView {
    
    fileprivate func getCard(index: Int) -> KCCardView {
        let cardView = self.cardAtIndex!(index) as! KCCardView
        return cardView
    }
    
    fileprivate func updateViews() {
        // Animation visible card
        for (index, card) in self.visibleCards.enumerated() {
            self.animateArrangeView(card, index, self.visibleCards, self)
        }
    }
    
    fileprivate func insertNextCard(cardView: KCCardView) {
        cardView.attachView()
        self.containerView.addSubview(cardView)
        self.containerView.sendSubview(toBack: cardView)
        self.visibleCards.append(cardView)
    }
    
    fileprivate func removeCard(cardView:KCCardView) {
        cardView.removeFromSuperview()
        if let _index = self.visibleCards.index(of: cardView) {
            self.visibleCards.remove(at: _index)
        }
        
    }
    
    fileprivate func willRemoveCard(cardView:KCCardView) {
        cardView.state = .removing
        if let _index = self.visibleCards.index(of: cardView) {
            self.visibleCards.remove(at: _index)
        }
    }
    
    fileprivate func insertPreviousCard(cardView: KCCardView) {
        cardView.attachView()
        self.containerView.addSubview(cardView)
        self.visibleCards.insert(cardView, at: 0)
    }
    
    fileprivate func applyLeftTransform(_ view: UIView) {
        let width = self.bounds.width
        let height = self.bounds.height
        let distance = max(width, height)

        
        var transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        transform = transform.translatedBy(x: distance, y: 0)
        transform = transform.rotated(by: (CGFloat.pi / 2))
        view.transform = transform
    }
    
}

//MARK: Public function
extension KCSwipeCardsView {
    
    public func loadCards() {
        if self.cardAtIndex != nil {
            for index in self.visibleCards.count..<self.numberOfCards {
                if self.visibleCards.count < self.numberOfVisibleCards {
                    let cardIndex = currentCardIndex + index
                    if cardIndex < self.numberOfCards {
                        let cardView = getCard(index: cardIndex)
                        insertNextCard(cardView: cardView)
                    }
                }
            }
        }
        // need time to autolayout fix constrains
        let when = DispatchTime.now() + 0.05
        DispatchQueue.main.asyncAfter(deadline: when, execute: { [unowned self] in
            self.updateViews()
        })
    }
    
    open func dequeueReusableCard(withIdentifier identifier: String, for index: Int) -> UIView {
        for subview in self.subviews {
            if subview is KCCardView {
                let cardView = subview
                if cardView.responds(to: #selector(getter: KCCardView.cardIdentifier)) {
                    let unmanagedObject: Unmanaged<AnyObject> = cardView.perform(#selector(getter: KCCardView.cardIdentifier))
                    let cardIdentifer: String = unmanagedObject.takeRetainedValue() as! String
                    if  cardIdentifer == identifier {
                        let reuseCardView = cardView.copy()
                        if cardView.responds(to: #selector(KCCardView.prepareForReuse)) {
                            cardView.perform(#selector(KCCardView.prepareForReuse))
                        }
                        return reuseCardView as! UIView
                    }
                }
            }
        }
        return KCCardView()
    }
    
    func popCard() -> KCCardView? {
        if self.currentCardIndex < self.numberOfCards - 1 {
            if let cardView = self.visibleCards.first {
                if cardView.state != .removing {
                    self.willRemoveCard(cardView: cardView)
                    self.currentCardIndex += 1
                    self.loadCards()
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseOut], animations: {
                        self.applyLeftTransform(cardView)
                    }, completion: { [unowned self] (bool) in
                        self.removeCard(cardView: cardView)
                    })
                    return cardView
                }
            }
        }
        return nil
    }
    
    
    func pushCard() -> KCCardView? {
        if self.currentCardIndex > 0 {
            let previousIndex = self.currentCardIndex - 1
            let cardView = self.getCard(index: previousIndex)
            
            if self.visibleCards.count == self.numberOfVisibleCards && self.visibleCards.last != nil {
                self.removeCard(cardView: self.visibleCards.last!)
            }
            self.currentCardIndex = previousIndex
            self.insertPreviousCard(cardView: cardView)
            self.applyLeftTransform(cardView)
            self.updateViews()
            return cardView
        }
        return nil
    }

}

// MARK: Gesture
extension KCSwipeCardsView {
    
    func handleSwipeLeft(gesture: UISwipeGestureRecognizer) {
        let _ = popCard()
    }
    
    func handleSwipeRight(gesture: UISwipeGestureRecognizer) {
        let _ = pushCard()
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        
        if self.currentCardIndex < self.numberOfCards {
            if let cardView = self.visibleCards.first {
                if cardView.state != .removing {
                    let touchPoint = gesture.location(in: cardView)
                    if cardView.bounds.contains(touchPoint) {
                        animateFlipView(cardView)
                    }
                }
            }
        }
    }
}


//MARK: Animation
extension KCSwipeCardsView {
    fileprivate static func arrangeAnimator() -> (_ view: KCCardView, _ index: Int, _ views: [UIView], _ swipeableView: KCSwipeCardsView) -> () {
        func toRadian(_ degree: CGFloat) -> CGFloat {
            return degree * CGFloat(M_PI / 180)
        }
        
        func rotateView(_ view: UIView, forDegree degree: CGFloat, duration: TimeInterval, offsetFromCenter offset: CGPoint, swipeableView: KCSwipeCardsView,  completion: ((Bool) -> Void)? = nil) {
            UIView.animate(withDuration: duration, delay: 0, options: .allowUserInteraction, animations: {
                view.center = swipeableView.convert(swipeableView.center, from: swipeableView.superview)
                var transform = CGAffineTransform(translationX: offset.x, y: offset.y)
                transform = transform.rotated(by: toRadian(degree))
                transform = transform.translatedBy(x: -offset.x, y: -offset.y)
                view.transform = transform
            }, completion: completion)
        }
        
        return { (view: KCCardView, index: Int, views: [UIView], swipeableView: KCSwipeCardsView) in
            let degree = CGFloat(2)
            let duration = 0.4
            let offset = CGPoint(x: 0, y: swipeableView.bounds.height * 0.3)
            view.state = .animating
            switch index {
            case 0:
                rotateView(view, forDegree: 0, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView, completion: { (Bool) -> Void in
                    view.state = .idle
                    view.showFrontView()
                })
            case 1:
                rotateView(view, forDegree: degree, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView, completion: { (Bool) -> Void in
                    view.state = .idle
                    view.showFrontView()
                })
            case 2:
                rotateView(view, forDegree: -degree, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView, completion: { (Bool) -> Void in
                    view.state = .idle
                    view.showFrontView()
                })
            default:
                rotateView(view, forDegree: 0, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView, completion: { (Bool) -> Void in
                    view.state = .idle
                    view.showFrontView()
                })
            }
        }
    }
    
    fileprivate static func flipAnimator() -> (_ view: KCCardView) -> () {
        func toRadian(_ degree: CGFloat) -> CGFloat {
            return degree * CGFloat(M_PI / 180)
        }
        
        func TransformForFlipingLayer(_ layer: CALayer, angle: CGFloat, offset: CGFloat) -> CATransform3D {
            var transform = CATransform3DIdentity
            transform.m34 = -0.002
            transform = CATransform3DTranslate(transform, 0, 0, offset)
            transform = CATransform3DRotate(transform, angle, 0, 1, 0)
            return transform
        }
        
        func TransformForTransitionLayer(_ layer: CALayer, offset: CGFloat) -> CATransform3D {
            var transform = CATransform3DIdentity
            transform.m34 = -0.002
            transform = CATransform3DTranslate(transform, 0, 0, offset)
            return transform
        }
        
        return { (view: KCCardView) in
            let layer = view.layer
            let offset = layer.bounds.width / 2
            let flipEndRadian = toRadian(-90)
            let flipBeginRadian = toRadian(90)
            let duration = 0.1
            let flipDuration = 0.2
            layer.transform = TransformForFlipingLayer(layer, angle: toRadian(0), offset: 0)
            
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseIn, animations: {
                layer.transform = TransformForTransitionLayer(layer, offset: offset)
            }, completion: { (Bool) in
                UIView.animate(withDuration: flipDuration, delay: 0.0, options: .curveEaseIn, animations: {
                    layer.transform = TransformForFlipingLayer(layer, angle: flipEndRadian, offset: offset)
                }, completion: {(Bool) in
                    layer.transform = TransformForFlipingLayer(layer, angle: flipBeginRadian, offset: offset)
                    view.switchContentView()
                    UIView.animate(withDuration: flipDuration, delay: 0.0, options: .curveEaseOut, animations: {
                        layer.transform = TransformForFlipingLayer(layer, angle: 0, offset: offset)
                    }, completion: { (Bool) in
                        UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseIn, animations: {
                            layer.transform = TransformForTransitionLayer(layer, offset: 0)
                        }, completion:nil)
                    })
                })
            })
        }
    }
}
