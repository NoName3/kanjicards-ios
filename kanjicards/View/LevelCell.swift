//
//  LevelCell.swift
//  kanjicards
//
//  Created by Kha Nguyen on 5/30/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell {
    
    public var value:ActionItem? {
        didSet {
            if let _titleLabel = self.titleLabel {
                _titleLabel.text = value?.title
            }
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var themeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.themeView.backgroundColor = UIColor.sky()
    }
}
