//
//  ActionItem.swift
//  KanjiSticker
//
//  Created by Kha Nguyen on 11/8/16.
//  Copyright © 2016 Kha Nguyen. All rights reserved.
//

import UIKit

class ActionItem {
    var title:String
    var subTitle:String?
    var subTitle2:String?
    var data: AnyObject?
    var action:((ActionItem) -> Void)?
    var actionAtIndex:((_ action:ActionItem, _ index: IndexPath) -> Void)?
    init() {
        title = ""
    }
    
    init(title:String, subTitle:String) {
        self.title = title
        self.subTitle = subTitle
    }
    
    init(title:String, subTitle:String, subtitle2:String) {
        self.title = title
        self.subTitle = subTitle
        self.subTitle2 = subtitle2
    }
}

class QuestionItem {
    var question:String
    var correctAnswer:Int
    var answer:Int?
    var answers:[ActionItem]?
    var referenceWord:Word?
    init() {
        question = ""
        correctAnswer = -1
    }
    
    init(question:String, correctAnswer:Int, answers:[ActionItem], referenceWord: Word) {
        self.question = question
        self.correctAnswer = correctAnswer
        self.answers = answers
        self.referenceWord = referenceWord
    }
}
