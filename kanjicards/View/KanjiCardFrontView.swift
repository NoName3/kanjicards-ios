//
//  KanjiCardFrontView.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/23/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class KanjiCardFrontView: KCCardContentView {

    
    @IBOutlet weak var kanjiLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    var showDetail:(() -> Void)?
    
    fileprivate var swipeupGestureRecognizer: UISwipeGestureRecognizer?
    
    override func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override func setup() {
        super.setup()
        self.swipeupGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeUp(gesture:)))
        self.swipeupGestureRecognizer?.direction = .up
        self.addGestureRecognizer(self.swipeupGestureRecognizer!)
    }
    
    deinit {
        self.removeGestureRecognizer(self.swipeupGestureRecognizer!)
    }

}


// MARK: Gesture
extension KanjiCardFrontView {
    
    func handleSwipeUp(gesture: UISwipeGestureRecognizer) {
        if let _showDetail = self.showDetail {
            _showDetail()
        }
    }
}
