//
//  TransparentHeader.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/30/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class CustomHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var themeView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
