//
//  WordDetailCell.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/26/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordDetailCell: UITableViewCell {
    
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
