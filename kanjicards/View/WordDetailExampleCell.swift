//
//  WordDetailExampleCell.swift
//  kanjicards
//
//  Created by Kha Nguyen on 4/26/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordDetailExampleCell: UITableViewCell {
    
    
    @IBOutlet weak var hiraganaLabel: UILabel!
    @IBOutlet weak var kanjiLabel: UILabel!
    @IBOutlet weak var shortMeanLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
