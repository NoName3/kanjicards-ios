//
//  WordCell.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/19/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class WordCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var themeView: UIView!
    public var value:ActionItem? {
        didSet {
            if let _titleLabel = self.titleLabel {
                _titleLabel.text = value?.title
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.themeView.backgroundColor = UIColor.fern()
    }

    

}
