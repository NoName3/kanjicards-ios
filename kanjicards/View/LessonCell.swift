//
//  LessonCell.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/16/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class LessonCell: UITableViewCell {

    public var value:ActionItem? {
        didSet {
            if let _titleLabel = self.titleLabel {
                _titleLabel.text = value?.title
            }
            if let _subTitleLabel = self.subTitle1Label {
                if let _subTitle = value?.subTitle {
                    _subTitleLabel.text = _subTitle
                }
            }
            if let _subTitleLabel = self.subTitle2Label {
                if let _subTitle = value?.subTitle2 {
                    _subTitleLabel.text = _subTitle
                }
            }
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitle1Label: UILabel!
    @IBOutlet weak var subTitle2Label: UILabel!
    @IBOutlet weak var themeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.themeView.backgroundColor = UIColor.aqua()
    }

}
