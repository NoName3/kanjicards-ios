//
//  KanjiCardBackView.swift
//  kanjicards
//
//  Created by Kha Nguyen on 6/23/17.
//  Copyright © 2017 Kha Nguyen. All rights reserved.
//

import UIKit

class KanjiCardBackView: KCCardContentView {
    
    
    @IBOutlet weak var kanjiLabel: UILabel!
    @IBOutlet weak var hanLabel: UILabel!
    @IBOutlet weak var kanjiNumberLabel: UILabel!
    @IBOutlet weak var shortMeanLabel: UILabel!
    @IBOutlet weak var showDetailButton: UIButton!
    
    fileprivate var swipeupGestureRecognizer: UISwipeGestureRecognizer?
    
    var showDetail:(() -> Void)?
    
    override func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override func setup() {
        super.setup()
        self.swipeupGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeUp(gesture:)))
        self.swipeupGestureRecognizer?.direction = .up
        self.addGestureRecognizer(self.swipeupGestureRecognizer!)
    }
    
    deinit {
        self.removeGestureRecognizer(self.swipeupGestureRecognizer!)
    }
    
    @IBAction func showDetailButtonDidTouch(_ sender: UIButton) {
        if let _showDetail = self.showDetail {
            _showDetail()
        }
    }

}

// MARK: Gesture
extension KanjiCardBackView {
    
    func handleSwipeUp(gesture: UISwipeGestureRecognizer) {
        if let _showDetail = self.showDetail {
            _showDetail()
        }
    }
}
